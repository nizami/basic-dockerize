#!/bin/sh

rm -rf basic
git submodule init
git submodule update
docker-compose down --rmi=local
docker-compose up -d --build
docker exec -it basicdockerize_php_1 composer install --no-interaction
chmod -R 777 .